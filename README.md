# README #

Used to provide a custom page for 503 errors.

### How to update ###

1. Make changes to the repo and push
2. Login on docker 'docker login --username=rebs'
3. Build and push image to docker 'docker build . --tag rebs/errorpage:latest && docker push rebs/errorpage:latest'
4. Update Rancher Stack on CRM Prod and Maximo Prod: stack in under the 'load-balancer' and it is called 'errorpage'